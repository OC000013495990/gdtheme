<?php

declare(strict_types=1);

namespace Csp\GdTheme;

use Shopware\Core\Framework\Plugin;
use Shopware\Storefront\Framework\ThemeInterface;

class GdTheme extends Plugin implements ThemeInterface
{
}
