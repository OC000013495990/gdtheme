<?php

declare(strict_types=1);

namespace Csp\GdTheme\Service;

use Shopware\Core\Content\Product\Events\ProductSearchCriteriaEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddProductSearchData implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            ProductSearchCriteriaEvent::class => 'addData',
        ];
    }

    public function addData(ProductSearchCriteriaEvent $event): void
    {
        $event->getCriteria()->addAssociation('properties');
        $event->getCriteria()->addAssociation('properties.group');
    }
}
