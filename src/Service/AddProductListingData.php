<?php

declare(strict_types=1);

namespace Csp\GdTheme\Service;

use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddProductListingData implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            ProductListingCriteriaEvent::class => 'addData',
        ];
    }

    public function addData(ProductListingCriteriaEvent $event): void
    {
        $event->getCriteria()->addAssociation('properties');
        $event->getCriteria()->addAssociation('properties.group');
    }
}
